---
layout: post
date: '2021-12-31 15:34:08 +0100'
title: At PhenomX, we diagnose health for her 
comment: current article is in _posts !
disabled: true
base: ../../../
preview-img: tablet-continuum.svg
author:
 name: Colleen Draper
 bio: colleen
---

Western medicine practice is focused on diagnosing disease from symptoms,
blood tests, physical exam, radiography, etc. This is important for
very sick patients to understand their best treatments. However,
these treatments are focused on the end of their health-disease
trajectory.<!-- more --> Individuals that experience symptoms earlier in their
health trajectory are not diagnosable with disease. They may have a
unique combination of symptoms that are uncomfortable and affect their
quality of life. They actively seek out medical care but often find few
effective answers. These symptoms reflect cellular imbalances. If they
can be remedied, the individual is more likely to return to a high state
of health and wellness and create a nutrition and lifestyle therapy that
reduces their risk of disease.

Women with symptoms related to hormonal health imbalances are in need of a health diagnoses. Their symptoms reflect the physiologic stress of changing hormones and they are a warning she needs to re-balance her nutrition and lifestyle. Nutrition therapies can be used to re-balance her health trajectory and much work needs to be done in this area to understand what works best. 

At PhenomX, we start with what we know and fill in the knowledge gaps through our customer journeys. Please follow us to learn more.
