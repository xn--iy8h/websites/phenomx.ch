---
title: Where the name PhenomX comes from ?

date: "2021-12-09 14:34:54 +0100"
disabled: true
author:
 name: "Anon Nymous"
layout: post
base: '../../../'
slug: why-phenomx
teaser-img: teaser.svg
preview-img: preview.svg
---
{%- if page.title == nil -%}
### Where the name PhenomX comes from ?
{%- endif %}



Our CEO is a woman and a mom with two X chromosomes :

so it was natural to call her creation "Fem Mom X";
then with some (base) science added (pH > 7), it becomes

**PhenomX**


