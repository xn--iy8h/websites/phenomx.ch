---
layout: post
date: Thu Dec  9 11:36:03 AM CET 2021
title: true non invasive health
draft: true
header: medecine and freedom of choice
teaser-img: teaser.jpg
preview-img: non-invasive
author:
 bio: michel
 name: "Michel C."
base: ../../../
---
{%- if page.title == nil -%}
## What is a true non invasive approach to health
{%- endif %}

Cure comes from an healthy mind-set, and non invasive method are essential to a prompt return to health.

Non invasive means you have a choice, and we are listenning

Your own organism intelligence know the paths back to health and has already triggered all it takes
to get you back to your full health potential. We developped methods to get better are listening
to your needs.

We believe in nuturing the "eco-system" rather that forcing the "nature hands" 
so we help providing the raw material for our internal "pharmacy"
to operate smoothly and timely.

Non-invasive health leaves your sovereign self the choice to reach the necessary equilibrium
using all the natural and technological forces at play.

This is what true health is, the whole system respond to the your needs and phemonx is part of it.
any symptoms are indicators of these needs, and phenomx gives you the tools to understand and
to apply an "wholistic" remedy to your very personal situation, without compromizing
your privacy or any medical secret.


Non invasive approach is innately respectful of life within,
which gives you awarness of your health needs when they manifest
our technology is a guide to highlight the cause to be addressed.


At every level, we are Phenomx promote non-invasive solution, even down this website:
we don't take you email, we don't push any content to you,
you always have the freedom of choice to get informed, learn about our methods,
acquire the knowledge about your health and selfcare.
And you will always be self-sovereign and in charge of you and your choices.

We do not collect data, you data and privacy is always protected,
moreover we don't have any access to it unless you share it with us.

we match your needs with what we learnt and give you back the gift of health awareness.




