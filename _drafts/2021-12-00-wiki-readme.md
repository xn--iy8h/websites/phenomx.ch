---
layout: post
title: a simple readme as a test
slug: wiki-readme
tic: 1639476204041
date: 2021-12-31 15:34:08 +0100
#thumbnail: readme-thumb.png
preview-img: readme-photo.jpg
author: unknown
base: ../../../
---
## readme

When posting to [gitlab wikis][1], a simple markdown file is created,
in order to convert this into a blog post, we need to add some metadata:

 - title
 - thumbnail
 - author,
 - date of publishing

The title and the thumbnail can be extracted from the wiki page itself,
while the author and date can be obtained with the commit log.

[1]: https://gitlab.com/xn--iy8h/websites/phenomx.ch/-/wikis
