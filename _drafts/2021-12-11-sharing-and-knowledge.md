---
title: "sharing-and-knowledge"
date: "2021-12-11 07:51:42 +0100"
disabled: true
author:
 name: "Michel C."
 bio: michel
layout: post
base: '../../../'
slug: sharing-and-knowledge
teaser-img: teaser.svg
preview-img: preview.svg
---
{%- if page.title == nil -%}
## Sharing knowledge
{%- endif %}

In a Scarcity based world, economy is additive (linear):
when you give you "substract" and "add", when you share you "divide", (decay)
energy is finite, entropy, everything eventually ends.


In a Knowledge based world, economy is multiplication (exponential):
when you give you "duplicate", when you share you "multiply" (growth)
energy is abundant, syntropy, everything regenerates.




