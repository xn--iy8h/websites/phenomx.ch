---
layout: post
title: Turning a table into an app
modal: true
date: 2021-12-13 12:22:08 +0100
draft: true
base: ../../../
---
{%- if page.title == nil -%}
## table to app !
{%- endif %}

-- We serve customer who prefer to invest in health than spend doctor bills.
so we develop this app for preventing health.


We use excel to drive the application architecture, in fact there are several tools
on the market that will speed up the app development with a "no-code" approach :


- <https://www.quora.com/How-do-I-convert-an-Excel-spreadsheet-into-an-app?share=1>
- <https://www.goodbarber.com/blog/convert-an-excel-file-into-a-mobile-app-a988/>
- 

see also:
 - [glide](https://www.glideapps.com/)
 - [flipabit](https://flipabit.dev/pricing/)
 - [clappia](https://www.clappia.com/)
 - [appery.io](https://appery.io/)
 - [udemy](http://www.udemy.com/spreadsheet-to-mobile-app/)
 - [bubbleio](https://bubble.io/)
 - [expo](https://expo.dev/)
 - [appizy](https://www.appizy.com/)
 - [apsheet][1] ([youtube example w/ appsheet](https://www.youtube.com/watch?v=IRuA3upKkOA))
 - [openasapp][2]
 - [mendix](https://www.mendix.com/pricing/)


[1]: https://solutions.appsheet.com/excel-to-app
[task]: https://www.appsheet.com/Template/AppDef?appName=ASTTaskTrackerAppDemo-1374619#Home
[2]: https://www.openasapp.com/how-to-turn-an-excel-spreadsheet-into-an-app/
[3]: https://mailpoof.com/mailbox/basin@mailpoof.com
