---
layout: post
date: 2021-12-31 15:34:08 +0100
author:
 name: Doctor I·T
 bio: michel
base: ../../../
preview-img: secret-photo.jpg
---
# PhenomX IT secrets

If you enjoy the content on our site, and you would like to share some,
in your term, i.e. styled differently; as long as you state the "origin"
of the article or content (i.e. by putting a link back to PhenomX),
we are comfortable with it.<!-- more -->

We are happy to provide you a json url for you to get the raw machine readable content for your own enjoyments.

All the pages: [here](data:application/json;base64,{{site.pages | jsonify | base64_encode | strip_newlines}})

---

{% for p in site.pages %}
{{forloop.index}}. [{{p.title | default: p.name}}]({{page.base}}{{p.url | replace_first: '/'}})
{% endfor %}


