#

# using credential from ~/.netrc
# and simple ftp protocol

git submodule update _wiki
git add _wiki
if [ "x$(uname -n)" = 'xocean' ]; then

export JEKYLL_ENV=production
jekyll build
#sh bin/qrsite.sh
# files are uploaded to infomaniak ftp server
# via safewatch ... due to double NAT problem

#rsync -auP _site/ safewatch:phx/

login=$(perl -S getnetrc.pl -u phenomx.ch)
pass=$(perl -S getnetrc.pl -p phenomx.ch)
#grep -v '^#' <<EOT | ssh safewatch 'cd phx; pwd; ftp -n'
cd _site
host=phenomx.ch
grep -v '^#' <<EOT | lftp
open $host
user $login $pass
#passive
#verbose
#prompt
#hash
#newer
mirror -r 2022
mput -d team/*.html
#mdel images/*.jpg
#mput images/caligenix-logo.png
mput -d images/*.webp
mput -d images/*.svg
mput *.html
put  index.html
mput -d css/wf-*.css
put style.css
mput -d js/*.js
mirror -r 2021
bye
EOT

fi

echo "xdg-open  https://phenomx.ch"

# testing scp
# scp _site/colors.html $login@phenomx.ch:/
