
const api_url = 'https://ipfs.safewatch.ml/api/v0/';
var tic = Date.now();

submit({});

let form = document.getElementsByTagName('form')[0];
console.log('form:',form);
form.addEventListener('change',fupdate,true);

let date = document.getElementsByName('ts')[0];
    date.valueAsDate = new Date();

function fupdate(ev) {
  console.log('fupdate.ev:',ev);
  let el = ev.target;
  console.log('fupdate.el:',el);
  let elapsed = Math.round((Date.now() - tic ) / 60);
  document.getElementById('elapsed').innerText = 'elapsed time: '+ elapsed / 1000 + 'min.';
  document.getElementsByName('elapsed')[0].value = elapsed * 60 / 1000;

  let rankmenu = document.getElementById(el.name+'r');
  console.log('fupdate.rankmenu:',rankmenu);
  if (typeof(rankmenu) != 'undefined') {
     console.log(el.name+'.checked:',el.checked);
     if (el.value != '' || el.checked) {
        console.log(el.name+':',el.value);
        rankmenu.style.display = ''; // display it w/o using block
     } else {
        rankmenu.style.display = 'none';
     }
  }
}

function hupdate(ev) { // height ...
  let hcm = ev.target.value * 2.54;
  document.getElementsByName('hcm')[0].value = hcm;
  bmiupdate();
  return hcm;
}
function wtupdate(ev) { // weight ...
  let wtkg = ev.target.value / 2.205;
  document.getElementsByName('wtkg')[0].value = wtkg;
  bmiupdate();
  return wtkg;
}
function bmiupdate(){ // bmi ...
  let wt = document.getElementsByName('wtkg')[0].value;
  let h = document.getElementsByName('hcm')[0].value / 100;
  let bmi = wt / Math.pow(h,2);
  document.getElementsByName('bmi')[0].value = Math.round(bmi*10)/10;
  return bmi;
}



async function submit(ev) {
  let tofu = await gettofu();
  console.log('tofu:',tofu);
  let name = document.getElementsByName('user')[0].value;
  let nid = getnid(`${name}@$tofu`)
  console.log('nid:',nid);
  let drit_pkey = '1234'; // <--- update here
  let secret = dh_secret(drit_pkey);
  let key = derivekey(secret,nid);
  console.log('key:',key);
  
}

function dh_secret(pkey) {
  return pkey; // TODO
}
function derivekey(priv,salt) {
  return salt+priv; // TODO
}

function getnid(s) {
  let sha2 = sha256(s); // returns hex 
  let ns36 = BigInt('0x'+sha2).toString(36).substr(0,13)
  return ns36
}

async function gettofu() {
  var tofu = localStorage.getItem('tofu');
  if (typeof(tofu) == 'undefined' || tofu == null) {
     tofu = await getspot();
     localStorage.setItem('tofu',tofu);
  }
  return tofu;
}

function getspot() {
  let tics = Math.floor(tic/1000)
  console.log('tic:',tics.toString(16));
  return getip().
   then(ip => {
     console.log('ip:',ip.toString(16));
     let spot = ip ^ tics
     return spot
  
   }).
   catch(console.warn);
}

function getip() {
  return fetch('https://ipfs.blockring™.ml/cgi-bin/remote_addr.txt').
   then(resp => resp.text()).
   //then(b => { console.log(b); return b; }).
   then( ip => { let nip = 0
    let parts = ip.split(".");
    for(let i = 0; i < 4; i++) {
      let partVal = Number(parts[i]);
      nip = (nip << 8) + partVal;
    }
    return nip;
   }).
   catch(console.error);
}
