## Hormonal health nutrition rejuvenation

My transition through peri-menopause has taught me how to make the best
decisions for myself, my family, the people I love. My lesson has been
"self" must come first. This is the case with many women who are mothers
and/or women who are committed to caring for the people
they love. Love the self first, then give to others.

**PhenomX Health** offering is for women who wish to know more about
their personal experience during this life transition and how to treat
their health symptoms using natural therapies for a healthy and empowered
aging experience.

Would you like to support the research needed to build this knowledge further?


We would like to use your
de-identified data to analyze how we can respond better to your needs for
optimal solutions, we will publish the research in peer reviewed journals that
will move this health field forward.

Thank you.

[Dr. Draper](https://www.nutrauhealth.com)
