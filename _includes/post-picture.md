{%- include liquid.html -%}
{%for post in site.posts%}
 {% if post.id == page.id %}
 {% assign pos = forloop.index0 %}
 {% endif %}
{% endfor %}
<!-- This article is #{{pos}} -->
{% assign twowords = page.slug | replace: '-',' ' | split: ' ' | slice: 0, 2 | join: ' ' %}<br>
![article{{pos}}: {{twowords}} photo]({{gwurl}}/ipfs/{{site.data.ipfs.qmroot}}/assets/{{page.preview-img}})

