#

export JEKYLL_ENV=production
git submodule update _wiki
rm -rf .jekyll-cache
jekyll build --trace --verbose -d _site 
du -a _site | grep -e '\.html' | sort -n | tail -10

#sh bin/qrsite.sh


exit $?

true;
