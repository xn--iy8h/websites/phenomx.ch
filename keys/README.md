---
layout: page
---
# git crypt commands cheatsheet



```sh
## git crypt ...
ls -l .git/git-crypt/keys
git-crypt init
dirname=${PWD##*/}
git-crypt export-key /keybase/private/$USER/sym_keys/git-crypt-${dirname}.key
ls -l $(git rev-parse --git-dir)/git-crypt/keys/default
cat > $(git rev-parse --show-toplevel)/.gitattributes <<EOT
*jwt* filter=git-crypt diff=git-crypt
*.sec* filter=git-crypt diff=git-crypt
*.key* filter=git-crypt diff=git-crypt
secrets/** filter=git-crypt diff=git-crypt
.gitattributes !filter !diff
EOT
git add .gitattributes
git commit -m "Configure git-crypt to encrypt confidential files"
git crypt status 

git crypt unlock
git crypt lock
git-crypt export-key -- - | gpg --symmetric --armor --output local.key.asc
git add local.key.asc
echo RELOADAGENT | gpg-connect-agent # flush gpg password
gpg --decrypt local.key.asc | xxd
gpg --decrypt local.key.asc | git-crypt unlock -
```

