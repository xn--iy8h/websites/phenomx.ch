#

top=$(git rev-parse --show-toplevel)
ip=$(localip.pl)
gwport=$(ipfs config Addresses.Gateway | cut -d'/' -f 5)
echo "--- # local ip and port" > $top/_data/local.yml
echo "ip: $ip" >> $top/_data/local.yml
echo "gwport: $gwport" >> $top/_data/local.yml

# -----------------------------------------------------
# local site ...
url=http://$ip:8088/websites/usb/phenomx.ch/_site/
echo url: $url >> $top/_data/local.yml
echo url: $url
qrurl="https://api.safewatch.xyz/api/v0/qrcode?url=$url&size=3"
echo qrurl: $qrurl
curl -s -o $top/images/qrlocal.png $qrurl
xdg-open "http://$ip:8088/websites/usb/phenomx.ch/images/qrlocal.png"
# -----------------------------------------------------
# ipfs site ...
qm=$(ipfs add -r $top/_site --ignore assets -Q)
url=http://$ip:$gwport/ipfs/$qm
echo qmurl: $url >> $top/_data/local.yml
echo url: $url
echo url: https://ipfs.io/ipfs/$qm
echo url: https://ipfs.safewatch.care/ipfs/$qm/styleguide.html
qrencode -o $top/images/qrsite.png "$url"
qrurl="https://api.safewatch.xyz/api/v0/qrcode?url=$url&size=3"
echo qrurl: $qrurl
#curl -s -o $top/images/qrsite.png $qrurl
eog $top/images/qrsite.png


exit $?


true;
