#

# intent: publish *.pdn


echo "--- # ${0##*/}"
find . -name '*~1' -delete

tic=$(date +%s%N | cut -c-13)
top=$(git rev-parse --show-toplevel)
cd $top/PDNs

qm=$(ipfs add -w *.pdn -Q --cid-version=0 --cid-base=base58flickr)
echo qm: $qm
ipfs files stat /ipfs/$qm
echo url: https://ipfs.safewatch.tk/ipfs/$qm
echo url: https://gateway.ipfs.io/ipfs/$qm

# Pin Services API ...
if ipfs swarm addrs local; then
  ipfs pin remote add --service=nft --name=phenomx $qm &
  if ipfs pin remote service ls | grep -w phenomx -q; then
    jwt=$(cat $top/keys/pinata_api_keys.jwt_token.yml | grep -w jwt | tail -1 | cut -d' ' -f2)
    ipfs pin remote service add phenomx https://api.pinata.cloud/psa $jwt
  fi
  ipfs pin remote rm --force --service=phenomx --name=pdns
  ipfs pin remote add --service=phenomx --name=pdns $qm &
  echo url: https://gateway.pinata.cloud/ipfs/$qm
else
  echo ipfs is offline
fi

top=$(git rev-parse --show-toplevel)
echo $tic: $qm >> qm.log
git add qm.log *.pdn
git commit -m "pdns published under $qm on $(date)"

exit $?
true;
