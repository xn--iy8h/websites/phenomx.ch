---
---
<meta charset="utf8">

<input name=rank type=range min=0 max=4 value=2 step=1 list=tickmarks> <span id=value><i>:value</i><span>
<datalist id=tickmarks>
<option>0</option>
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
</datalist>

<script>
document.getElementsByName('rank')[0].addEventListener('change',update,false);

function update(ev) {
 let e = ev.target;
 console.log('e:',e);
 document.getElementById('value').innerText = e.value;
 return e.value;
}
</script>

