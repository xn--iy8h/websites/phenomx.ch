---
layout: post
order: 2
date: '2021-12-26 10:53:26 +0100'
title: Health Optimization for Her 
featured: true
base: ../../../
preview-img: cabbage-compass.jpg
preview-img: health-disease-continuum.svg
inside-img: iw-continuum-earth.svg
author:
 name: Dr. Draper
 bio: colleen
---
{% include post-picture.md %}

How do we optimize health? Let's start with some perspective.

Western medicine practice is focused on diagnosing disease from symptoms,
blood tests, physical exam, radiography, etc. This is important for
very sick patients to understand their best treatments. However,
these treatments are focused on the end of their health-disease
trajectory<!-- more -->. Individuals that experience symptoms earlier in their
health trajectory are not diagnosable with disease. They may have a
unique combination of symptoms that are uncomfortable and affect their
quality of life. They actively seek out medical care but often find few
effective answers. These symptoms reflect cellular imbalances. If they
can be remedied, the individual is more likely to return to a high state
of health and wellness and create a nutrition and lifestyle therapy that
reduces their risk of disease.

Women with symptoms related to hormonal health imbalances are in need of health optimization. Their symptoms reflect the physiologic stress of changing hormones and they are a warning she needs to re-balance her nutrition and lifestyle. The transiiton of perimenopause to menopause is a 10 year time frame, on average, that is characterized by hormone changes and imbalances. Seventy percent of women in this stage of life experience life disrupting symptoms, such as heavy and eratic periods, hot flashes, sleep disturbances and anxiety. Seventy five percent of these women remain untreated after seeking medical attention.

Nutrition therapies can be used to re-balance her health trajectory. Responses are highly personalized and much work needs to be done in this area to understand what works best. 

At PhenomX, we start with what we know and fill in the knowledge gaps through our customer journeys. Please follow us to learn more.
