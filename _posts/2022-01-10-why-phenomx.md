---
title: "why phenomx?"
date: "2022-01-09 18:13:00 +0100"
disabled: false
author:
 name: "Dr. Draper"
 bio: colleen
layout: post
base: '../../../'
slug: why-phenomx
teaser-img: teaser.svg
preview-img: dance.webp
---
{% include post-picture.md %}

Perimenopause is the transitional time to menopause that can last 8-10 years before menstruation ceases. It is most common to start this transition in her 40s and end in her 50s. During this time, she may experience new and different health symptoms that reflect her transition to a new hormonal equilibrium. It is officially over once she has 12 months without a period but she may continue to have symptoms a few years past this time. Everyone’s experience during this life transition is unique. 

Women entering the stage of peri-menopause are in the process of creation of a new life rhythm. This is why it is sometimes uncomfortable. The more she is open to change, the better the outcome. However, this does not mean the journey is easy. The outcome is better if she is open to the challenges, discomforts and unexpected solutions. The truth is a full investment in the journey guarantees discomfort. This discomfort comes with an upside. It is an opportunity to establish a deep knowing of self and a strong comfort with life. 

Our PhenomX platform is for women who wish to know more about their personal experience during this life transition and how to treat their health symptoms using nutrition therapies for a healthy and empowered aging experience.
