#

echo "--- # ${0##*/}"
slug=$(echo "$*" | sed -e 's/ /-/g' | tr '[:upper:]' '[:lower:]')
slug="${slug:-new-article}"
echo slug: $slug

stamp=$(date +"%Y-%m-%d")
if [ -e $stamp-$slug.md ]; then
stamp=$(date +"%Y-%m-00")
fi
date=$(date +'%Y-%m-%d %H:%M:%S %z')

find . -name '*~1' -delete
if [ ! -e $stamp-$slug.md ]; then
cat <<EOT > $stamp-$slug.md
---
title: "$*"
date: "$date"
disabled: false
author:
 name: "Anon Nymous"
layout: post
base: '../../../'
slug: $slug
teaser-img: teaser.svg
preview-img: preview.svg
---
## $*

We believe $1 ...


You may wonder why "$*" ?
Well let me tell you ...

EOT
else
  echo "Error: $date-$slug.md exists !"
fi
