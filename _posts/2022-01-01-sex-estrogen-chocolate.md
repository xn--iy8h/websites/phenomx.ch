---
title: "Sex, Estrogen, and Chocolate"
date: "2022-01-01 09:20:18 +0100"
disabled: false
author: 
 name: "Dr. Draper"
 bio: colleen
layout: post
base: '../../../'
slug: sex-estrogen-chocolate
teaser-img: teaser.svg
preview-img: chocolate-types.webp
---
{% include post-picture.md %}


Our PhenomX headquarters is in Switzerland, where we have some of the
most delicious chocolate in the world. With a chocolaterie in nearly
every village and multiple in every city, living here has taught me a
new appreciation for the divine taste of chocolate and the endless ways
it can be made and enjoyed. Choosing high cocoa dark chocolate (70-85%
cocoa) is easy to rationalize since it has some great health benefits
from its rich content of bioactive nutrients, including polyphenols,
flavanols and catechins.<!-- more --> 

In postmenopausal women, dark chocolate attenuates subsequent food intake when compared to milk and white chocolate [[1]]; and
improves blood flow to the brain and reduces inflammation in
the arteries [[2]]. Its effect on improvement in blood circulation can also
lead to an improvement in sex drive which often decreases for women
during perimenopause. High cocoa dark chocolate flavanols may slow down
the visual skin aging women start experiencing during this time. This
is because cocoa intake has been shown to protect the skin from the
harmful effects of sunlight, while improving blood flow to the skin and
increasing skin hydration and density [[3]].

Did you know chocolate increases estrogen in the body? Estrogenic foods, such as soy and chocolate, can be helpful to reduce low estrogen related symptoms, such as irregular menstrual cycles, mood swings and hot flashes. High cocoa dark chocolate also makes us feel good by triggering the release of endorphins, serotonin, dopamine and oxytocin that are considered the “feel good” neurotransmitters. In women suffering from low neurotransmitter production which can occur in the setting of low estrogen, chocolate may help mitigate this situation. 

Variations in the gene for oxytocin have been associated with higher food intake and waist circumference [[4]]. Perimenopausal and menopausal women suffering from low oxytocin who also have these gene variations may experience a more exaggerated desire for chocolate to mitigate their symptoms.

As a perimenopausal women living in Switzerland, I have also had to learn how to turn my chocolate craving into something that is healthful and satisfying. 
My favorite option is to combine 85% dark chocolate with cocoa nibs to extend the flavor. Sometimes, I also throw in some gogi berries or plain coconut flakes to extend the good nutrition. 

I hope this article has inspired
you to choose high cocoa dark chocolate as a part of your healthy diet
strategy on your menopausal journey towards rejuvenation and longevity.


[1]: https://pubmed.ncbi.nlm.nih.gov/28572069/
[2]: https://pubmed.ncbi.nlm.nih.gov/30552275/
[3]: https://pubmed.ncbi.nlm.nih.gov/30663066/
[4]: https://pubmed.ncbi.nlm.nih.gov/34091593/

