#!/usr/bin/perl

use Text::CSV qw(csv);
use YAML::Syck qw(DumpFile Dump);
my $intent = "convert csv to form w/ Love";

my $file = shift;
my ($fpath,$fname,$bname,$ext) = &fname($file);

my $sm = '"'; # string markers
my $cs = ','; # comma separator
my $csv = Text::CSV->new ({ binary => 1, sep_char => $cs, quote_char => $sm });

open my $fh, "<:encoding(utf8)", $file or die "$file: $!";
while (my $row = $csv->getline ($fh)) {
    #$row->[2] =~ m/pattern/ or next; # 3rd field should match
    push @rows, $row;
    }
close $fh;
DumpFile('rows.yml',\@rows);
print "file: rows.yml\n";

my @aoh = (); # Array of hash
my @headers = @{$rows[0]};
foreach my $ri (1 .. $#rows) {
   my @field = @{$rows[$ri]};
   #printf "%s\n",Dump(\@field);
   my %hash = ( map( { $headers[$_] => $field[$_]; } (0 .. $#field) ) );
   #printf "%s\n",Dump(\%hash);
   push @aoh, \%hash;
   #printf "%d: [%s]%d\n",$ri,join(',',@field),$#field+1;
   foreach my $j (0 .. $#field) {
      my $col = chr(ord('A') + $j);
      my $key = sprintf'%s%03d',$col,$ri;
      $db{$key} = $field[$j];
   }
}
DumpFile('form.yml',\%db);
DumpFile('aoh.yml',\@aoh);
print "file: form.yml\n";
print "file: aoh.yml\n";


# generating the form ...
local *HTM;
open HTM,'>:encoding(utf8)','form.html';
printf HTM qq'<form action="mailto:Dr.Draper\@PhenomX.ch?Subject:+PhenomX%27;+Questionnaire" method=post>\n';


my $select = 0;
foreach my $ri (0 .. $#rows) {
  my $row = $rows[$ri];
  my $q = $aoh[$ri];

  if ($select && $q->{tag} eq 'option' ) {
    $q->{choices} =~ s/’/'/g;
    #printf "select.option: @%d: %s\n",$ri,$q->{choices};
    if ($q->{type} eq 'label') {
      printf HTM '<option label="%s" value="" style="%s" %s>%s</option>'."\n",
         $q->{choices},$q->{style},$q->{attributes},$q->{choices};
    } else {
      printf HTM '<option value="%s" style="%s" %s>%s</option>'."\n",
         $q->{value},$q->{style},$q->{attributes},$q->{choices};
    }
  } else {
     $q->{attributes} =~ s/\\x94/"/g;
     $q->{attributes} =~ s/\\x92/'/g;
     if ($select) {
      #printf "select: @%d: tag: %s\n",$ri,$q->{tag};
       print  HTM "</select>\n";
       $select = 0;
     }
     if ($q->{cat} ne '' ) {
       if ($checklist && $q->{cat} eq 'question') {
         print HTM "</ul><br clear=all>\n"; # closing checklist...
         $checklist = 0;
       }
       if ($q->{flag} eq 'skip') { next; }
       if ($q->{cat} !~ /^(?:sub|rank)/) {
          printf HTM qq'<hr title="line %d">',$ri+2;
       }
       if ($q->{cat} =~ /^sub/) { # sub-question ...
          printf HTM qq'\n<br>';
       }
       if ($q->{cat} =~ m/\bquestion/) {
         $qi++;
         printf HTM qq'<span title="#%d">Q%d:</span> ',$ri+2,$qi;
       } else {
         printf HTM '<span class="sect invisible">&#xa7;%d:</span> ',$ri+2; # section sign/symbol
       }
     } else {
       if ($q->{flag} eq 'skip') { next; }
     }
     if ($q->{description} ne '') {
        $q->{description} =~ s/\\x94/"/g;
        $q->{description} =~ s/\\x92/'/g;
        print HTM $q->{description},"\n";
        if ($q->{cat} =~ /\bquestion/ && $q->{tag} eq 'select') {
           printf HTM qq'\n<br>';
        }
     }

     if ($q->{type} eq 'radio') {
        if (defined $q->{value} && $q->{value} ne '') {
           printf HTM '<br><input type="%s" name="%s" placeholder="%s" value="%s" title="%s" style="%s" %s>%s'."\n",
                  $q->{type},$q->{name}, $q->{placeholder}, $q->{value},$q->{hint},$q->{style},$q->{attributes},$q->{choices};
        }
     } elsif ($q->{tag} eq 'input' && $q->{type} eq 'checkbox') {
        if ($checklist) { print HTM '<li>'; }
        printf HTM '<input type="%s" name="%s" placeholder="%s" value="%s" title="%s" style="%s" %s> %s <span id="%s_value"></span>'."\n",
               $q->{type},$q->{name}, $q->{placeholder}, $q->{value},$q->{hint},$q->{style},$q->{attributes},$q->{choices},$q->{name};
     } elsif ($q->{tag} eq 'input' && $q->{type} eq 'range') {
        printf HTM '<div id="%s" style="%s"><input type="%s" name="%s" onclick="%s_update(event)" value="%s" title="%s" %s> %s <span id="%s_value"></span></div>'."\n",
               $q->{name},$q->{style},$q->{type},$q->{name}, $q->{name}, $q->{value},$q->{hint},$q->{attributes},$q->{choices},$q->{name};

     } elsif ($q->{tag} eq 'input' && $q->{cat} eq 'question') {
        printf HTM '<input type="%s" name="%s" placeholder="%s" value="%s" title="%s" style="%s" %s> %s'."\n",
               $q->{type},$q->{name}, $q->{placeholder}, $q->{value},$q->{hint},$q->{style},$q->{attributes},$q->{unit};
     } elsif ($q->{tag} eq 'textarea') {
        printf HTM '<br><textarea name="%s" title="%s" style="%s" %s>%s</textarea>'."\n",
               $q->{name}, $q->{hint},$q->{style},$q->{attributes},$q->{value};
     } elsif ($q->{tag} eq 'checklist') {
        print HTM qq'<ul class="checklist">\n';
        $checklist++;
     } elsif ($q->{tag} eq 'select') {
        printf qq'DBUG ¶%d: q:%s\n',$ri+2,&nonl(Dump($q),','); # Pilcrow paragraph symbol
        $select++;
        printf "%d: select++ \n",$ri+2;
        if ($checklist) { print HTM '<br>'; }
        printf HTM '<select type="%s" id="%s" name="%s" placeholder="%s" value="%s" title="%s" style="%s" %s> %s'."\n",
               $q->{type},$q->{name},$q->{name}, $q->{placeholder}, $q->{value},$q->{hint},$q->{style},$q->{attributes},$q->{unit};
     } elsif ($q->{tag} eq 'button') {
        printf HTM '<button type="%s" name="%s" placeholder="%s" value="%s" title="%s" style="%s" %s>%s</button>'."\n",
               $q->{type},$q->{name}, $q->{placeholder}, $q->{value},$q->{hint},$q->{style},$q->{attributes},$q->{choices};
        # <a href=”mailto:piotr@mailtrap.io” target=”_blank” rel="noopener noreferrer”>I want free Bitcoins</a>
     } else {
        $q->{attributes} =~ s/\\x92/'/g;
        if ($q->{tag} ne '' && $q->{type} ne '') {
      
           printf HTM '<%s type="%s" name="%s" placeholder="%s" value="%s" title="%s" style="%s" %s>%s</%s>',$q->{tag},
                  $q->{type},$q->{name}, $q->{placeholder}, $q->{value},$q->{hint},$q->{style},$q->{attributes},$q->{choices},$q->{tag};
           if ($q->{unit} ne '') { printf HTM qq' %s\n',$q->{unit} } else { print HTM "\n" }
        }
     }
  }
}
print HTM qq'<span id=elapsed></span>\n';
print HTM qq'</form>\n';
print HTM qq'<style>ul.checklist{list-style-type: none;}.checklist>li{display:block;}</style>\n';
print HTM qq'<script src=app.js></script>\n';

close HTM;
print "file: form.html\n";


exit $?;

sub fname { # extract filename etc...
  my $f = shift;
  $f =~ s,\\,/,g; # *nix style !
  my $s = rindex($f,'/');
 return $t;
  my $fpath = '.';
  if ($s > 0) {
    $fpath = substr($f,0,$s);
  } else {
    use Cwd;
    $fpath = Cwd::getcwd();
  }
  my $file = substr($f,$s+1);
  if (-d $f) {
    return ($fpath,$file);
  } else {
  my $bname;
  my $p = rindex($file,'.');
  my $ext = lc substr($file,$p+1);
     $ext =~ s/\~$//;
  if ($p > 0) {
    $bname = substr($file,0,$p);
    $ext = lc substr($file,$p+1);
    $ext =~ s/\~$//;
  } else {
    $bname = $file;
    $ext = 'dat'
  }
  $bname =~ s/\s+\(\d+\)$//; # remove (1) in names ...

  return ($fpath,$file,$bname,$ext);

  }
}

sub nonl {
 my ($t,$s) = @_;
 $t =~ s/\r?\n/$s/g;
 return $t;
}

1;
