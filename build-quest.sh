#

top=$(git rev-parse --show-toplevel)
export PATH=$top/bin:$PATH
set -e
find . -name '*~1' -delete
libreoffice --convert-to csv "PhenomX_Recommendation.ods"
perl -S csv2form.pl "PhenomX_Recommendation.csv"
mv *.yml _data
mv form.html _includes/healthq.html

libreoffice --convert-to csv "PhenomX_Survey.ods"
perl -S csv2form.pl "PhenomX_Survey.csv"
mv *.yml _data
mv form.html _includes/surveyq.html

export JEKYLL_ENV=production
jekyll build --drafts

qm=$(ipfs add -r _site -Q)
perl -S fullname.pl -a $qm > _data/bot.yml
curl -I https://ipfs.safewatch.ml/ipfs/$qm
echo url: https://ipfs.safewatch.ml/ipfs/$qm
