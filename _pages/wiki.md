---
layout: page
permalink: wiki/
base: ../
---

{% assign wiki = site.collections | where: label, 'wiki' %}
## {{site.data.config.brand.name}} wiki:  {{site.wiki.size}} articles

qm: [{{site.data.ipfs.qmroot}}](https://ipfs.safewatch.care/ipfs/{{site.data.ipfs.qmroot}})
  
{% for article in site.wiki %}
##  {{article.title}} ([#{{forloop.index}}]({{page.base}}{{article.url|remove_first:'/'|append: '.html'}})):

  ![preview](https://gateway.pinata.cloud/ipfs/{{site.data.ipfs.qmroot}}/assets/{{page.preview-img}})
  
  > {{article.content|strip_html|strip_newlines|truncate: 160}}
  
  <a href="{{page.base}}{{article.url | remove_first: '/' | append: '.html'}}" class="button w-inline-block">read more</a>
  
- a machine readable [json file](data:application/json;base64,{{article|jsonify|base64_encode|strip_newlines}})

---
  
{%- endfor -%}




