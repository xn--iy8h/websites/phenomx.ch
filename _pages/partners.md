---
title: partners
permalink: "partners/"
header: our partners
layout: page
base: ../
---

## {{site.data.pages.partners.title}}


{% for partner in site.data.pages.partners.list %}
- [{{partner.name}}][{{forloop.index}}]
{% endfor %}
<!-- - [Eco Organic Technologies][eot] -->

{% for partner in site.data.pages.partners.list %}
[{{forloop.index}}]: {{partner.url}}
{% endfor %}

