---
layout: page
permalink: "mission.html"
title: "Phenomꭓ mission"
base: ./
---
{% assign config = site.data.content %}
{% assign basename = page.name | replace: '.md','' %}
{% assign pagedata = site.data.pages[basename] %}

{% if pagedata.title %}
## {{pagedata.title}}{% endif %}
{% if pagedata.sub-title %}
### {{pagedata.sub-title}}
{% if pagedata.content %}
{{pagedata.content}}
{% endif %}
{% endif %}

<!-- {{site.data.pages.vision.title}}
{{site.data.pages.vision.content}}
-->


[](data:application/json,{{pagedata|jsonify}})
